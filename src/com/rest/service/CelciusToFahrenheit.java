/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/ctofservice")
public class CelciusToFahrenheit {

  @GET
  @Path("/getFahrenheit")
  @Produces({
      MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON
  })
  @Consumes({
      MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON
  })
  public Double convertCelciustoFahrenheit() {

    Double fahrenheit;
    final Double celsius = 1.0;

    fahrenheit = celsius * 9 / 5 + 32;

    return fahrenheit;
  }

  @GET
  @Path("/getFahrenheit/{c}")
  @Produces({
      MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON
  })
  @Consumes({
      MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON
  })
  public Double convertCelciustoFahrenheitfromInput(@PathParam("c") Double c) {
    Double fahrenheit;
    final Double celsius = c;
    fahrenheit = celsius * 9 / 5 + 32;

    return fahrenheit;
  }
}